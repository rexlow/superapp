import React, { Component } from 'react';
import AppNavigator from "navigator/AppNavigator";
import { createAppContainer } from 'react-navigation';

const AppContainer = createAppContainer(AppNavigator)

class App extends Component {
  render() {
    return (
      <AppContainer />
    )
  }
}

export default App;