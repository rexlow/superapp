// must import react in order to render icons here!
import React, {Component} from 'react';

import { createStackNavigator, createBottomTabNavigator } from 'react-navigation';
import Ionicons from 'react-native-vector-icons/Ionicons';

// routes
import HomeScreen from "containers/home/HomeScreen";
import MapScreen from "containers/features/MapScreen";
import NetworkCall from "containers/features/NetworkCall";
import Sensor from "containers/features/Sensor";
import StateManagement from "containers/features/StateManagement";
import WebView from "containers/features/WebView";
import Movie from "containers/features/Movie";

// UI Navigator tabs
import UIElements from "containers/features/UIElements";
import Flexbox from "containers/features/Flexbox";
import UIComponents from "containers/features/UIComponents";

const UINavigatorTab = createBottomTabNavigator({
    UIElements: UIElements,
    Flexbox: Flexbox,
    UIComponents
}, {
        navigationOptions: {
            title: "UI Elements",
            headerStyle: {
                backgroundColor: "#7c4dff"
            },
            headerTintColor: '#fff',
            headerTitleStyle: {
                fontWeight: 'bold',
            }
        },
        defaultNavigationOptions: ({ navigation }) => ({
            tabBarIcon: ({ focused, horizontal, tintColor }) => {
                const { routeName } = navigation.state;
                let IconComponent = Ionicons;
                let iconName;
                if (routeName === 'UIElements') {
                    iconName = "ios-bulb";
                } else if (routeName === 'Flexbox') {
                    iconName = "ios-options"
                } else if (routeName === 'UIComponents') {
                    iconName = "ios-options"
                }

                // You can return any component that you like here!
                return <IconComponent name={iconName} size={25} color={tintColor} />;
            },
        }),
        tabBarOptions: {
            activeTintColor: "steelblue",
            inactiveTintColor: "gray",
            // showLabel: false
        }
    })

const AppNavigator = createStackNavigator({
    HomeScreen: HomeScreen,
    UINavigatorTab: UINavigatorTab,
    MapScreen: MapScreen,
    NetworkCall: NetworkCall,
    Sensor: Sensor,
    StateManagement: StateManagement,
    WebView: WebView,
    Movie: Movie
}, {
        initialRouteName: "HomeScreen",
        headerMode: "screen"
    });

export default AppNavigator;