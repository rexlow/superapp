import React, { Component } from "react";
import {
    Image,
    StyleSheet,
    Text,
    TouchableOpacity
} from "react-native";
import { withNavigation } from 'react-navigation';

class MovieItem extends Component {

    shouldComponentUpdate(nextProps, nextState) {
        if (nextProps.item.title === this.props.item.title) {
            console.log("same data, improve performance by disable rendering!")
            return false
        }
        return true
    }

    render() {
        const { API_IMAGE_URL, API_IMAGE_SIZE, item } = this.props;
        return (
            <TouchableOpacity
                onPress={() => this.props.navigation.navigate("Movie", this.props)}
                style={styles.movieItem}>
                <Image 
                    source={{ uri: `${API_IMAGE_URL}${API_IMAGE_SIZE}${item.poster_path}` }} 
                    style={styles.moviePoster} />
                <Text 
                    ellipsizeMode='tail'
                    numberOfLines={1}
                    style={{ textAlign: "center", marginTop: 7 }}>
                    {item.title}
                </Text>
            </TouchableOpacity>
        )
    }
}

const styles = StyleSheet.create({
    movieItem: {
        width: 100,
        height: 180,
        marginHorizontal: 15,
        marginVertical: 20
    },
    moviePoster: {
        width: 100,
        height: 175,
        borderRadius: 7
    },
})

export default withNavigation(MovieItem);