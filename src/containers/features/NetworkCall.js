import React, { Component } from "react";
import {
    ActivityIndicator,
    FlatList,
    Image,
    View,
    StyleSheet,
    Text,
    TouchableOpacity
} from "react-native";
import MovieItem from "components/MovieItem";

export default class NetworkCall extends Component {

    static navigationOptions = {
        title: "Movies",
        headerStyle: {
            backgroundColor: '#d50000',
        },
        headerTintColor: '#fff',
        headerTitleStyle: {
            fontWeight: 'bold',
        }
    }

    constructor(props) {
        super(props);
        this.state = {
            API_URL: "https://api.themoviedb.org/3",
            API_ENDPOINT: "discover/movie",
            API_KEY: "bc4344730175446d5bb89622abf0e6e3",
            API_IMAGE_URL: "https://image.tmdb.org/t/p/w",
            API_IMAGE_SIZE: 300, // image dimension
            PAGE: 1,
            movies: [],
            gotResult: null
        }
        this.getMoviesFromAPI = this.getMoviesFromAPI.bind(this);
        this.renderResults = this.renderResults.bind(this);
        this.renderMovies = this.renderMovies.bind(this);
    }

    componentDidMount() {
        this.getMoviesFromAPI()
    }

    async getMoviesFromAPI() {
        const { API_URL, API_ENDPOINT, API_KEY, PAGE } = this.state;
        try {
            const response = await fetch(`${API_URL}/${API_ENDPOINT}?api_key=${API_KEY}&page=${PAGE}`);
            const responseJSON = await response.json();
            this.setState({ movies: responseJSON.results, gotResult: true });
        } catch (error) {
            this.setState({ gotResult: false });
            console.error(error)
        }
    }

    renderMovies(movies) {
        return (
            <FlatList 
                data={movies}
                numColumns={3} // use this for flexWrap
                renderItem={({ item, index }) => (
                    <MovieItem 
                        item={item} 
                        API_IMAGE_URL={this.state.API_IMAGE_URL}
                        API_IMAGE_SIZE={this.state.API_IMAGE_SIZE} />
                )}
                columnWrapperStyle={styles.columnWrapperStyle}
                keyExtractor={(item, index) => `item-${item.id}`}  />
        )
    }

    renderResults() {
        const { gotResult, movies } = this.state;
        if (gotResult === null) {
            return <ActivityIndicator size="large" />
        } else if (gotResult) {
            if (movies.length > 0) {
                return this.renderMovies(movies);
            } else {
                return <Text>No movies was found.. Please try again in a while..</Text>
            }
        } else if (!gotResult) {
            return <Text>Something went wrong...</Text>
        }
    }

    render() {
        const { center, container } = styles;
        return (
            <View style={[container, center]}>
                {this.renderResults()}
            </View>
        )
    }
}

const styles = StyleSheet.create({
    center: {
        justifyContent: "center",
        alignItems: "center"
    },
    container: {
        flex: 1,
        flexDirection: "row",
        flexWrap: "wrap"
    },
    columnWrapperStyle: {
        flex: 1,
        justifyContent: "space-around"
    },
    movieItem: {
        width: 100,
        height: 180,
        marginHorizontal: 15,
        marginVertical: 20
    },
    moviePoster: {
        width: 100,
        height: 175,
        borderRadius: 7
    },
    absoluteView: {
        flex: 1,
        position: 'absolute',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'transparent'
    },
})