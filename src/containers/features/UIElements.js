import React, { Component } from "react";
import {
    View,
    StyleSheet,
    Text
} from "react-native";


export default class UIElements extends Component {

    // https://reactnavigation.org/docs/en/headers.html
    static navigationOptions = {
        title: "UI Elements",
        headerStyle: {
            backgroundColor: '#f4511e',
        },
            headerTintColor: '#fff',
            headerTitleStyle: {
            fontWeight: 'bold',
        },
    }

    render() {
        const { center, container, redContainer, yellowContainer, greenContainer, whiteContainer } = styles;
        return (
            <View style={container}>

                {/* by defauly flex direction is column */}
                <View style={[redContainer, center]}>
                    <View style={whiteContainer} />
                    <View style={whiteContainer} />
                    <View style={whiteContainer} />
                </View>

                {/* flex direction row */}
                <View style={[yellowContainer, center]}>
                    <View style={whiteContainer} />
                    <View style={whiteContainer} />
                    <View style={whiteContainer} />
                    <View style={whiteContainer} />
                    <View style={whiteContainer} />
                    <View style={whiteContainer} />
                </View>

                {/* flex wrap */}
                <View style={[greenContainer, center]}>
                    <View style={whiteContainer} />
                    <View style={whiteContainer} />
                    <View style={whiteContainer} />
                    <View style={whiteContainer} />
                    <View style={whiteContainer} />
                    <View style={whiteContainer} />
                    <View style={whiteContainer} />
                    <View style={whiteContainer} />
                    <View style={whiteContainer} />
                    <View style={whiteContainer} />
                    <View style={whiteContainer} />
                    <View style={whiteContainer} />
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    center: {
        justifyContent: "center",
        alignItems: "center"
    },
    container: {
        flex: 1,
    },
    redContainer: {
        flex: 0.2,
        backgroundColor: "red"
    },
    yellowContainer: {
        flex: 0.3,
        backgroundColor: "yellow",
        flexDirection: "row"
    },
    greenContainer: {
        flex: 0.5,
        backgroundColor: "green",
        flexDirection: "row",
        flexWrap: "wrap"
    },
    whiteContainer: {
        backgroundColor: "white",
        height: 75,
        width: 75,
        margin: 5
    }
})