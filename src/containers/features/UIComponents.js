import React, { Component } from "react";
import {
    Alert,
    AsyncStorage,
    Image,
    View,
    StyleSheet,
    Text,
    TextInput,
    TouchableOpacity
} from "react-native";


export default class UIComponents extends Component {

    // https://reactnavigation.org/docs/en/headers.html
    static navigationOptions = {
        title: "UI Components",
        headerStyle: {
            backgroundColor: '#f4511e',
        },
            headerTintColor: '#fff',
            headerTitleStyle: {
            fontWeight: 'bold',
        },
    }

    constructor(props) {
        super(props);
        this.state = {
            email: "",
            password: "",
            authenticated: false,
            mockEmail: "rex123@gmail.com",
            mockPassword: "12345678"
        }
        this.onSubmitPress = this.onSubmitPress.bind(this);
        this.onResetPress = this.onResetPress.bind(this);
    }

    componentWillMount() {
        this._retrieveData("authenticated")
            .then(res => {
                if (res) {
                    this.setState({ authenticated: true });
                }
            })
            .catch(err => console.error(err))
    }

    _storeData = async (key, value) => {
        try {
            await AsyncStorage.setItem(key, value);
        } catch (error) {
            console.error(error)
        }
    };

    _retrieveData = async (key) => {
        try {
            const value = await AsyncStorage.getItem(key);
            if (value !== null) {
                return value
            }
        } catch (error) {
            console.error(error)
        }
    };

    _removeData = async (key) => {
        try {
            await AsyncStorage.removeItem(key)
        } catch (error) {
            console.error(error)
        }
    }

    onSubmitPress() {
        const { email, password, mockEmail, mockPassword } = this.state;
        if (email === "" && password === "") {
            Alert.alert("Oops", "Field cannot be empty!")
        } else {
            Alert.alert("Please wait", `Logging ${email} in`, [
                {text: "OK", onPress: () => {
                    
                    // mock 1s delay
                    setTimeout(() => {
                        if (mockEmail === email && mockPassword === password) { 
                            this.setState({ authenticated: true, email: "", password: "" }, () => {
                                this._storeData("authenticated", true);
                                Alert.alert("Welcome back", "You are authenticated!");
                            })
                        }
                    }, 1000);
                }},
            ])
        }
    }

    onResetPress() {
        const { email, password } = this.state;
        if (email === "" && password === "") {
            Alert.alert("Hmm", "Field are already empty")
        } else {
            this.setState({
                email: "",
                password: ""
            })
        }
    }

    render() {
        const { center, container, textInputContainer, textInput, buttonContainer, button } = styles;
        return (
            <View style={container}>
                <Image 
                    source={{
                        uri: this.state.authenticated 
                                ? "https://facebook.github.io/react/logo-og.png"
                                : 'https://icon-library.net/images/my-profile-icon-png/my-profile-icon-png-3.jpg' }}
                    style={{
                        width: 120, 
                        height: 120,
                        borderRadius: 60,
                        alignSelf: "center",
                        marginVertical: 20
                    }} />

                <View style={textInputContainer}>
                    <TextInput 
                        keyboardType="email-address"
                        placeholder="Your email"
                        autoCapitalize="none"
                        style={textInput}
                        value={this.state.email}
                        onChangeText={email => this.setState({ email })} />
                    
                    <TextInput 
                        secureTextEntry={true}
                        placeholder="Your password"
                        autoCapitalize="none"
                        style={textInput}
                        value={this.state.password}
                        onChangeText={password => this.setState({ password })} />
                </View>
                
                <View style={buttonContainer}>
                    <TouchableOpacity 
                        style={[button, center]}
                        onPress={this.onSubmitPress}>
                        <Text>Submit</Text>
                    </TouchableOpacity>
                    <TouchableOpacity 
                        style={[button, center]}
                        onPress={this.onResetPress}>
                        <Text>Reset</Text>
                    </TouchableOpacity>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    center: {
        justifyContent: "center",
        alignItems: "center"
    },
    container: {
        flex: 1,
    },
    textInputContainer: {
        marginHorizontal: 20,
        justifyContent: "flex-start"
    },
    textInput: {
        height: 40,
        width: "100%",
        marginBottom: 10,
        borderColor: "skyblue",
        borderWidth: 2,
        borderRadius: 10,
        paddingLeft: 10
    },
    buttonContainer: {
        marginHorizontal: 20,
        flexDirection: "row",
        justifyContent: "space-around"
    },
    button: {
        height: 40,
        width: "30%",
        backgroundColor: "skyblue",
        borderRadius: 10,
    }
})