import React, { Component } from "react";
import { View, TouchableOpacity, Text } from "react-native";
import { WebView as WV } from 'react-native-webview';
import html from "../../data/index.html";


export default class Web extends Component {

    static navigationOptions = {
        title: "WebView",
        headerStyle: {
            backgroundColor: '#00796b',
        },
        headerTintColor: '#fff',
        headerTitleStyle: {
            fontWeight: 'bold',
        }
    }

    changeBodyColor(color) {
        const run = `document.body.style.backgroundColor = '${color}';
                     true;`;
        this.webview.injectJavaScript(run);
    }

    render() {

       
        return (
            <View style={{ flex: 1 }}>
                <WV
                    ref={ref => (this.webview = ref)}
                    originWhitelist={['*']} // allow locally served resources
                    source={html}/>
                <View style={{ flexDirection: "row" }}>
                    <TouchableOpacity 
                        onPress={() => this.changeBodyColor("yellow")}
                        style={{ 
                            height: 50, 
                            width: 200, 
                            backgroundColor: "#1565c0", 
                            borderRadius: 5, 
                            justifyContent: "center",
                            alignItems: "center" }}>
                        <Text style={{ color: "#fff" }}>Yellow</Text>
                    </TouchableOpacity>
                    <TouchableOpacity 
                        onPress={() => this.changeBodyColor("red")}
                        style={{ 
                            height: 50, 
                            width: 200, 
                            backgroundColor: "#1565c0", 
                            borderRadius: 5, 
                            justifyContent: "center",
                            alignItems: "center" }}>
                        <Text style={{ color: "#fff" }}>Red</Text>
                    </TouchableOpacity>
                </View>
            </View>
        )
    }
}