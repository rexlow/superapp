import React, { Component } from "react";
import {
    Dimensions,
    View,
    StyleSheet,
    Text
} from "react-native";

const { width, height } = Dimensions.get('window');

export default class Flexbox extends Component {
    render() {
        const { container, title, box } = styles;
        return (
            <View style={container}>
                
                {/* row, row-reverse, column, column-reverse */}
                <Text style={title}>Flex direction: row</Text>
                <View style={{ flexDirection: "row"}}>
                    <View style={[box, { backgroundColor: "#8e0038" }]} />
                    <View style={[box, { backgroundColor: "#c51162" }]} />
                    <View style={[box, { backgroundColor: "#fd558f" }]} />
                </View>

                {/* center, flex-start, flex-end, space-between, space-around */}
                <Text style={title}>Justify content: space-around</Text>
                <View style={{ flexDirection: "row", justifyContent: "space-around" }}>
                    <View style={[box, { backgroundColor: "#8e0038" }]} />
                    <View style={[box, { backgroundColor: "#c51162" }]} />
                    <View style={[box, { backgroundColor: "#fd558f" }]} />
                </View>

                
                <Text style={title}>Device Dimension</Text>
                <View style={{ backgroundColor: "#fd558f" }}>
                    <View style={[box, { backgroundColor: "#8e0038", width: width*0.75 }]} />
                    <View style={[box, { backgroundColor: "#c51162", width: width*0.45, height: 150 }]} />
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    title: {
        fontSize: 26,
        fontWeight: "300",
        marginVertical: 20,
        alignSelf: "center"
    },
    box: {
        height: 75,
        width: 75,
        margin: 5
    }
})