import React, { Component, Fragment } from "react";
import {
    ActivityIndicator,
    View,
    Text,
    TouchableOpacity
} from "react-native";
import MovieItem from "../../components/MovieItem";

export default class StateManagement extends Component {

    static navigationOptions = {
        title: "State Management",
        headerStyle: {
            backgroundColor: '#d50000',
        },
        headerTintColor: '#fff',
        headerTitleStyle: {
            fontWeight: 'bold',
        }
    }

    constructor() {
        super();

        // initial values
        this.state = {
            movies: [],
            movieUrl: "https://gist.githubusercontent.com/rexlow/96511d177863680b126a51fc217b3c40/raw/dfe5ff021badb773738e9ac26400fde91da3415c/movies.json",
            API_IMAGE_URL: "https://image.tmdb.org/t/p/w",
            API_IMAGE_SIZE: 300, // image dimension,
            count: 0
        }
        this.onPrevPressed = this.onPrevPressed.bind(this);
        this.onNextPressed = this.onNextPressed.bind(this);
    }

    UNSAFE_componentWillMount() {
        console.log("Component is about to mount. But too bad its going to be deprecated!")
    }

    componentDidMount() {
        console.log("Component mounted!")
        fetch(this.state.movieUrl)
            .then(res => res.json())
            .then(movies => this.setState({ movies }))
            .catch(err => console.log(err))
    }

    renderMovies(movies, index) {
        return (
            <View style={{ flexDirection: "row", justifyContent: "center" }}>
                <MovieItem 
                    item={movies[index]} 
                    API_IMAGE_URL={this.state.API_IMAGE_URL}
                    API_IMAGE_SIZE={this.state.API_IMAGE_SIZE} />
                <MovieItem 
                    item={movies[index+1]} 
                    API_IMAGE_URL={this.state.API_IMAGE_URL}
                    API_IMAGE_SIZE={this.state.API_IMAGE_SIZE} />
                <MovieItem 
                    item={movies[index+2]} 
                    API_IMAGE_URL={this.state.API_IMAGE_URL}
                    API_IMAGE_SIZE={this.state.API_IMAGE_SIZE} />
            </View>
        )
    }

    onPrevPressed() {
        if (this.state.count != 0) {
            this.setState({ count: this.state.count - 1});
        }
    }

    onNextPressed() {
        if (this.state.count != this.state.movies.length) {
            this.setState({ count: this.state.count + 1});
        }
    }

    render() {
        const { movies } = this.state;
        return (
            <View style={{ flex: 1 }}>
                {movies.length > 0 ? this.renderMovies(this.state.movies, this.state.count) : <ActivityIndicator />}
                <View style={{ 
                        flexDirection: "row", 
                        position: "absolute", 
                        left: 0,
                        bottom: 20,
                        right: 0,
                        justifyContent: "space-evenly"}}>
                    <TouchableOpacity
                        onPress={this.onPrevPressed}
                        style={{
                            backgroundColor: "#212121",
                            height: 50,
                            width: 140,
                            borderRadius: 3,
                            justifyContent: "center",
                            alignItems: "center"
                        }}>
                        <Text style={{ color: "#fff", fontSize: 26 }}>Previous</Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                        onPress={this.onNextPressed}
                        style={{
                            backgroundColor: "#d50000",
                            height: 50,
                            width: 140,
                            borderRadius: 3,
                            justifyContent: "center",
                            alignItems: "center"
                        }}>
                        <Text style={{ color: "#fff", fontSize: 26 }}>Next</Text>
                    </TouchableOpacity>
                </View>
            </View>
        )
    }
}