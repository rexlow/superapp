import React, { Component } from "react";
import {
    Alert,
    Dimensions,
    Image,
    View,
    StyleSheet,
    Text,
    TouchableOpacity
} from "react-native";
import { withNavigation } from 'react-navigation';

import StarRating from 'react-native-star-rating';
import IonIcon from 'react-native-vector-icons/Ionicons' 

const { width } = Dimensions.get("window");

class Movie extends Component {

    static navigationOptions = ({ navigation: { state } }) => {
        return {
            title: state.params.item.title,
            headerStyle: {
                backgroundColor: '#d50000',
            },
            headerTintColor: '#fff',
            headerTitleStyle: {
                fontWeight: 'bold',
            },
            headerRight: (
                <TouchableOpacity 
                    onPress={() => Alert.alert("Yay", "You've liked this movie!")}
                    style={{ marginRight: 16 }}>
                    <IonIcon name="ios-heart" color="#fff" size={24} />
                </TouchableOpacity>
            )
        }
    }

    render() {
        const { item, API_IMAGE_URL } = this.props.navigation.state.params;
        const { container, topContainer, backdropImage, posterContainer, poster, 
                movieInfoContainer, overviewContainer } = styles;
        return (
            <View style={container}>
                <Image 
                    style={backdropImage}
                    source={{ uri: `${API_IMAGE_URL}500${item.backdrop_path}` }} />
                <View style={topContainer}>
                    <View style={posterContainer}>
                        <Image 
                            source={{ uri: `${API_IMAGE_URL}500${item.poster_path}` }}
                            style={poster} />
                    </View>
                    <View style={movieInfoContainer}>
                        <Text style={{ fontSize: 20, fontWeight: "700" }}>{item.original_title}</Text>
                        <View style={{ width: width*0.4, marginTop: 10 }}>
                            <StarRating 
                                disabled={true} 
                                maxStars={5}
                                starSize={20}
                                halfStarEnabled={true}
                                fullStarColor={'red'}
                                rating={Math.round(item.vote_average/2)} />
                        </View>
                        <Text style={{ fontSize: 16, marginTop: 10 }}>Released on {item.release_date}</Text>
                    </View>
                </View>
                <View style={overviewContainer}>
                    <Text style={{ fontSize: 18, fontWeight: "700" }}>Background Story</Text>
                    <Text style={{ fontSize: 16, marginTop: 10 }}>{item.overview}</Text>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    topContainer: {
        flexDirection: "row",
        marginHorizontal: 15
    },
    backdropImage: {
        width: "100%", 
        height: 150
    },
    posterContainer: {
        position: "relative",
        top: -50,
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.8,
        shadowRadius: 5,
        elevation: 1,
    },
    poster: {
        height: 180,
        width: 120
    },
    movieInfoContainer: {
        margin: 10,
        width: width*0.6,
        justifyContent: "flex-start"
    },
    overviewContainer: {
        marginHorizontal: 15
    }
})

export default withNavigation(Movie);