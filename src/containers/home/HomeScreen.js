import React, { Component } from "react";
import {
    FlatList,
    View,
    Text,
    TouchableOpacity
} from "react-native";

const features = [
    { id: "0", name: "UI Elements", directory: "UINavigatorTab" },
    { id: "1", name: "Map Screen", directory: "MapScreen" },
    { id: "2", name: "Network Call", directory: "NetworkCall" },
    { id: "3", name: "Sensor", directory: "Sensor" },
    { id: "4", name: "State Management", directory: "StateManagement" },
    { id: "5", name: "WebView", directory: "WebView" },
]

export default class HomeScreen extends Component {

    constructor(props) {
        super(props);
        this.renderMenu = this.renderMenu.bind(this);
        this.renderFeature = this.renderFeature.bind(this);
    }

    // configure navigator by screen
    static navigationOptions = {
        // headerTitleStyle: { alignSelf: 'center' },
        // title: 'Super App',
        header: null,
        headerMode: 'none'
    }

    renderFeature({ item: { name, directory }}) {
        return (
            <TouchableOpacity 
                style={[styles.featureButton, styles.center]}
                onPress={() => this.props.navigation.navigate(directory)}>
                <Text style={styles.featureText}>{name}</Text>
            </TouchableOpacity>
        )
    }

    renderMenu() {
        return (
            <FlatList 
                data={features}
                renderItem={this.renderFeature}
                keyExtractor={(item, index) => item.id} />
        )
    }

    render() {
        return (
            <View style={[styles.container, styles.center]}>
                <View style={[styles.titleBox, styles.center]}>
                    <Text style={styles.titleText}>SuperApp 🚀</Text>
                </View>
                {this.renderMenu()}
            </View>
        )
    }
}

const styles = {
    center: {
        justifyContent: "center", 
        alignItems: "center"
    },
    container: {
        flex: 1, 
        height: "100%"
    },
    titleBox: {
        width: "100%",
        height: 60,
        marginTop: 80,
        marginBottom: 40
    },
    titleText: {
        fontSize: 40,
        fontWeight: "700",
        fontStyle: "italic"
    },
    featureButton: {
        height: 50, 
        width: 200, 
        backgroundColor: "#7c4dff",
        marginVertical: 20,
        transform: [{ rotate: '357deg'}]
    },
    featureText: {
        color: "white",
        fontSize: 20,
        fontStyle: "italic"
    }
}